// 'use strict';

const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const snap = document.getElementById("snap");
const errorMsgElement = document.querySelector('span#errorMsg');

const constraints = {
  audio: false,
  video: {
    width: 480, height: 320
  }
};

// Access webcam
async function init() {
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
  } catch (e) {
    errorMsgElement.innerHTML = 'navigator.getUserMedia error:${e.toString()}';
  }
}

// Success
function handleSuccess(stream) {
  window.stream = stream;
  video.srcObject = stream;
}

// Load init
init();

// Draw image
var context = canvas.getContext('2d');
snap.addEventListener("click", function() {
	context.drawImage(video, 0, 0, 480, 320);
	upload();
});

/*
** Partially nicked from https://gist.github.com/kylemcdonald/7873f9a5101a2537e190c96c38d36d44
*/

function canvasToBase64(canvas) {
	var fuckyou = canvas.toDataURL('image/jpeg');
	return fuckyou;
}
function upload() {
	canvasToBase64(canvas, function(b64) {
		b64 = b64.replace('data:image/jpeg;base64,', ''); // remove content type
		request = {
			"requests":[
				{
					"image":{ "content": b64 },
					"features":[
						{
							// if you want to detect more faces, or detect something else, change this
							"type":"FACE_DETECTION",
							"maxResults":1
						}
					]
				}
			]
		};
		jQuery.ajax({
			method: 'POST',
			url: 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyCp4g2KcZxhyX3U6QXTQ1guAUSwdolBYBA',
			contentType: 'application/json',
			data: JSON.stringify(request),
			processData: false,
			success: function(data){
				output = data;
				var faceData = data.responses[0].faceAnnotations[0];
				console.log('joy: ' + faceData.joyLikelihood);
				console.log('sorrow: ' + faceData.sorrowLikelihood);
				console.log('anger: ' + faceData.angerLikelihood);
				console.log('surprise: ' + faceData.surpriseLikelihood);
			},
			error: function (data, textStatus, errorThrown) {
				console.log('error: ' + data);
			}
		});
	});
}