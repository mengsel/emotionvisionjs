// 'use strict';

const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const snap = document.getElementById("snap");
const errorMsgElement = document.querySelector('span#errorMsg');
const constraints = {
  audio: false,
  video: {
    width: 480, height: 320
  }
};

// Access webcam
async function init() {
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
  } catch (e) {
    errorMsgElement.innerHTML = 'navigator.getUserMedia error:${e.toString()}';
  }
}

// Success
function handleSuccess(stream) {
  window.stream = stream;
  video.srcObject = stream;
}

// Load init
init();

// Draw image
var context = canvas.getContext('2d');
var clearDelay = 6000;
jQuery(snap).on('click touchstart', function() {
	lool();
	setTimeout(function () {
		context.drawImage(video, 0, 0, 480, 320);
		upload();
		setTimeout(function () {
			context.clearRect(0, 0, canvas.width, canvas.height);
		}, clearDelay);
	}, 1800);
});

/*
** LOOL
*/
function lool() {
	var items = [".img1",".img2",".img3",".img4",".img5"];
	randomElement = items[Math.floor(Math.random()*items.length)];
	jQuery(randomElement).show(0).delay(2400).hide(0);
}

/*
** Partially nicked from https://gist.github.com/kylemcdonald/7873f9a5101a2537e190c96c38d36d44
*/

function canvasToBase64(canvas) {
	console.log('in canvas func');
	var b64img = canvas.toDataURL('image/jpeg');
	return b64img;
}
function upload() {
	console.log('in upload func');
	var b64 = canvasToBase64(canvas);
	b64 = b64.replace('data:image/jpeg;base64,', ''); // remove content type
	console.log(b64); 
	request = {
		"requests":[
		{
			"image":{
				"content": b64
			},
			"features":[
			{
				// if you want to detect more faces, or detect something else, change this
				"type":"FACE_DETECTION",
				"maxResults":1
			}
			]
		}
		]
	};
	jQuery.ajax({
		method: 'POST',
		url: 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyCp4g2KcZxhyX3U6QXTQ1guAUSwdolBYBA',
		contentType: 'application/json',
		data: JSON.stringify(request),
		processData: false,
		success: function(data){
			output = data;
			console.log("succes response");
			console.log(data);
			var faceData = data.responses[0].faceAnnotations[0];
			
			var angerLH = faceData.angerLikelihood;
			var joyLH = faceData.joyLikelihood;
			var surpriseLH = faceData.surpriseLikelihood;
			var sorrowLH = faceData.sorrowLikelihood;
			
			console.log('joy: ' + faceData.joyLikelihood);
			console.log('sorrow: ' + faceData.sorrowLikelihood);
			console.log('anger: ' + faceData.angerLikelihood);
			console.log('surprise: ' + faceData.surpriseLikelihood);
			jQuery('#somresponse').show().delay(clearDelay).hide(0);
			
			if (angerLH == 'VERY_LIKELY' || angerLH == 'LIKELY') {
				console.log('No need to be angry menn; Smile more, worry less! :winking_face_with_tongue:\U0001f600');
				jQuery('#somresponse .x-text-content-text-primary').html('<p>&#128545;</p>'); 
				jQuery('#somresponse .x-text-content-text-subheadline').text('No need to be angry menn; Smile more, worry less!');

			} else if (joyLH == 'VERY_LIKELY' || joyLH == 'LIKELY') {
				console.log('Do you know Expelliarmus? Because your smile is disarming. :grinning_face_with_big_eyes:');
				jQuery('#somresponse .x-text-content-text-primary').html('<p>&#128514;</p>'); 
				jQuery('#somresponse .x-text-content-text-subheadline').text('Do you know Expelliarmus? Because your smile is disarming.');

			} else if (surpriseLH == 'VERY_LIKELY' || surpriseLH == 'LIKELY') {
				console.log('You seem surprised!? \U0001F600');
				jQuery('#somresponse .x-text-content-text-primary').html('<p>&#128558;</p>');
				jQuery('#somresponse .x-text-content-text-subheadline').text('You seem surprised!?');

			} else if (sorrowLH == 'VERY_LIKELY' || sorrowLH == 'LIKELY') {
				console.log('No need to be sad, its a PARTEY :rainbow::cake:');
				jQuery('#somresponse .x-text-content-text-primary').html('<p>&#128557;</p>');
				jQuery('#somresponse .x-text-content-text-subheadline').text('No need to be sad, its a PARTAY');

			} else {
				console.log('Are you a poker player? I cannot read your facial expression');
				jQuery('#somresponse .x-text-content-text-primary').html('<p>&#128526;</p>');
				jQuery('#somresponse .x-text-content-text-subheadline').text('Are you a poker player? I cannot read your facial expression');
			}; 
			
		},
		error: function (data, textStatus, errorThrown) {
			console.log('error: ' + data);
		}
	});
}